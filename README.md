<img src="spec.png" width="100%" />

 - "Headlines" app uses the Guardian's news API to fetch headlines and display them to the user. Users can favourite articles that they like.

### Guardian API Key

- Note that you might need to replace the Guardian API key for this project if [this link](https://content.guardianapis.com/search?api-key=09658731-cb6d-4a84-9e3c-5f030389de4e) doesn't work. To get a new key, go to https://bonobo.capi.gutools.co.uk/register/developer and register yourself as a developer. Once you get your key, replace the existing one in `Article.swift` with your own and you should be good to go!
