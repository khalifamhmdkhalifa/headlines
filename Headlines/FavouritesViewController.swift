//
//  FavouritesViewController.swift
//  Headlines
//
//  Created by Joshua Garnham on 09/05/2017.
//  Copyright © 2017 Example. All rights reserved.
//

import UIKit
import SDWebImage

class SubtitleCell: UITableViewCell {
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView?.sd_cancelCurrentImageLoad()
    }
}

final class FavouritesViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    var articleSelected: ((IndexPath) -> Void)?
    private var articles: [Article]!
    private var fileterdArticles: [Article]!
    private let favouritesManager: FavouritesManager
    
    init(articles: [Article], favouritesManager: FavouritesManager) {
        self.articles = articles
        fileterdArticles = articles
        self.favouritesManager = favouritesManager
        super.init(nibName: "FavouritesViewController", bundle: nil)
        try? favouritesManager.listenToFavouritesUpdates(listenerName: Self.description(), onUpdate: { [weak self] _ in
            guard let self = self else {
                return
            }
            self.onFavouritesUpdate()
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) is not supported")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonPressed))
        tableView.register(SubtitleCell.self, forCellReuseIdentifier: "cell")
        tableView.delegate = self
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @objc func doneButtonPressed() {
        dismiss(animated: true, completion: nil)
    }
    
    func onFavouritesUpdate() {
        articles.removeAll(where: {
            !favouritesManager.isFavourite(articleId: $0.id)
        })
        fileterdArticles.removeAll(where: {
            !favouritesManager.isFavourite(articleId: $0.id)
        })
        
        tableView.reloadData()
    }
    
    deinit {
        favouritesManager.removeListener(listenerName: Self.description())
    }
}

extension FavouritesViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // TODO: Perform search
        guard !searchText.isEmpty else {
            fileterdArticles = articles
            tableView.reloadData()
            return
        }
        let lowercasedSearchText = searchText.lowercased()
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            guard let self = self else { return }
            let fileterdArticles = articles.filter { $0.lowercaseHeadline.contains(lowercasedSearchText) }
            DispatchQueue.main.async { [weak self] in
                self?.fileterdArticles = fileterdArticles
                self?.tableView.reloadData()
            }
        }
    }
}

extension FavouritesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fileterdArticles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let article = fileterdArticles[indexPath.row]
        cell.textLabel?.text = article.headline
        cell.detailTextLabel?.text = article.published?.description
        cell.imageView?.sd_setImage(with: article.imageURL,
                                    completed: { [weak cell] _,_,_,_ in
            cell?.setNeedsLayout()
        })
        
        return cell
    }
}

extension FavouritesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let article = fileterdArticles[indexPath.row]
        let pager = ArticlesContainerViewController.createViewController(articles: fileterdArticles,visibleArticle: article, presentedByFavourites: true)
        self.navigationController?.pushViewController(pager, animated: true)
    }
}
