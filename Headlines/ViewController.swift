//
//  ViewController.swift
//  Headlines
//
//  Created by Joshua Garnham on 09/05/2017.
//  Copyright © 2017 Example. All rights reserved.
//

import UIKit
import SDWebImage

final class ViewController: UIViewController {
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var headlineLabel: UILabel!
    @IBOutlet var starButton: UIButton!
    @IBOutlet var bodyLabel: UILabel!
    // should be injected instead
    var favouritesManager: FavouritesManager = FavouritesManagerImpl.shared
    var article: Article?
    var presentedByFavourites: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // need to move the logic from here and make the VC passive.
        
        if let article = article {
            // article already loaded
            updateViews(article: article)
            return
        }
        
        reload()
        Article.fetchArticles { _, _ in
            self.reload()
        }
        
        favouritesManager.refresh { [weak self] in
            if let self = self, let article = self.article {
                self.updateStartButton(isFavourite: favouritesManager.isFavourite(articleId: article.id))
            }
        }
    }
    
    func reload() {
        guard let article = Article.all.first else { return }
        self.article = Article(article: article)
        updateViews(article: article)
    }
    
    private func updateViews(article: Article) {
        headlineLabel.text = article.headline
        bodyLabel.text = article.body
        imageView.sd_setImage(with: article.imageURL)
        updateStartButton(isFavourite: favouritesManager.isFavourite(articleId: article.id))
    }
    
    @IBAction func favouritesButtonPressed() {
        guard !presentedByFavourites else {
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        favouritesManager.getAllFavourites(completion: { [weak self] result in
            guard let self = self, !result.isEmpty else { return }
            let viewController = FavouritesViewController(articles: result,
                                              favouritesManager: favouritesManager)
            let nc = UINavigationController(rootViewController: viewController)
            self.present(nc, animated: true, completion: nil)
        })
    }
    
    @IBAction func starButtonPressed() {
        // we should move the logic from the vc.
        guard let article = article else { return }
        let isFavourite = favouritesManager.isFavourite(articleId: article.id)
        if isFavourite {
            favouritesManager.removeFromFavourites(articleId: article.id)
        } else {
            favouritesManager.addToFavourites(articleId: article.id)
        }
        updateStartButton(isFavourite: favouritesManager.isFavourite(articleId: article.id))
    }
    
    func updateStartButton(isFavourite: Bool) {
        UIView.transition(with: starButton, duration: 0.4, options: .transitionFlipFromLeft, animations: {
            if isFavourite {
                self.starButton.setBackgroundImage(UIImage(named: "favourite-on"), for: .normal)
            } else {
                self.starButton.setBackgroundImage(UIImage(named: "favourite-off"), for: .normal)
            }
        })
    }
}

extension ViewController {
    static func createArticleViewController(article: Article? = nil,
                                             presentedByFavourites: Bool = false) -> ViewController {
        let viewController = ViewController()
        viewController.presentedByFavourites = presentedByFavourites
        viewController.article = article
        return viewController
    }
}
