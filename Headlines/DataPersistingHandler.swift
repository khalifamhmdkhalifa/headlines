//
//  DataPersistingHandler.swift
//  Headlines
//
//  Created by khalifa on 13/08/2023.
//  Copyright © 2023 Example. All rights reserved.
//

import Foundation

protocol DataPersistingHandler {
    func store<T>(data: T, key: String, completion: (() -> Void)?) where T: Encodable
    func getData<T: Decodable>(key: String, type: T.Type, completion: @escaping (Result<T, Error>)->Void)
}

final class DataPersistingHandlerImpl: DataPersistingHandler {
    enum DataPersistingError: Error {
        case unknownError(String)
    }
    
    func store<T>(data: T, key: String, completion: (() -> Void)?) where T: Encodable {
        DispatchQueue.global(qos: .userInitiated).async {
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(data) {
                let defaults = UserDefaults.standard
                defaults.set(encoded, forKey: key)
            }
        }
    }
    
    func getData<T: Decodable>(key: String, type: T.Type, completion: @escaping (Result<T, Error>)->Void) {
        let defaults = UserDefaults.standard
        guard let savedPerson = defaults.object(forKey: key) as? Data
        else {
            completion(.failure(DataPersistingError.unknownError("couldn't find any data for the given key")))
            return
        }
        
        let decoder = JSONDecoder()
        if let data = try? decoder.decode(T.self, from: savedPerson) {
            completion(.success(data))
        } else {
            completion(.failure(DataPersistingError.unknownError("Failed To Decode the Data.")))
        }
    }
}
