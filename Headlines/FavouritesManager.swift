//
//  FavouritesManager.swift
//  Headlines
//
//  Created by khalifa on 13/08/2023.
//  Copyright © 2023 Example. All rights reserved.
//

import Foundation
import RealmSwift

protocol FavouritesManager {
    func isFavourite(articleId: String) -> Bool
    func addToFavourites(articleId: String)
    func removeFromFavourites(articleId: String)
    func getAllFavourites(completion: @escaping ([Article])->Void)
    func refresh(completion: (() ->Void)?)
    func listenToFavouritesUpdates(listenerName: String, onUpdate: @escaping ([Article])->Void) throws
    func removeListener(listenerName: String)
}

// not thread safe!!
final class FavouritesManagerImpl {
    private var favouritesIds: Set<String> = []
    private var dataPersistingHandler: DataPersistingHandler = DataPersistingHandlerImpl()
    private var listeners = [String:(([Article])->Void)]()
    static private(set) var shared = FavouritesManagerImpl()
    private static let favouritesKey = "favourites"
    
    private init() {
        refresh(completion: nil)
    }
    
    func refresh(completion: (() ->Void)?) {
        dataPersistingHandler.getData(key: Self.favouritesKey,
                                      type: Array<String>.self,
                                      completion: { result in
            switch result {
            case .success(let data):
                self.favouritesIds = Set(data)
            case .failure(_):
                break
            }
            completion?()
        })
    }
    
    private func onUpdate() {
        getAllFavourites { [weak self] favourites in
            guard let self = self else { return }
            for listener in self.listeners.values {
                listener(favourites)
            }
        }
        
    }
    
    private class ListenerNameAlreadyUsedError: Error {
        
    }
}


extension FavouritesManagerImpl: FavouritesManager {
    
    func getAllFavourites(completion: @escaping ([Article]) -> Void) {
        DispatchQueue.global(qos: .userInitiated).async {
            let favourites = Article.all
                .filter{ self.favouritesIds.contains($0.id)}
                .map { Article(article: $0) }
            DispatchQueue.main.async {
                completion(favourites)
            }
        }
    }
    
    func listenToFavouritesUpdates(listenerName: String,
                                   onUpdate: @escaping ([Article]) -> Void) throws {
        guard listeners[listenerName] == nil else {
            throw ListenerNameAlreadyUsedError()
        }
        listeners[listenerName] = onUpdate
    }
    
    func removeListener(listenerName: String) {
        listeners[listenerName] = nil
    }
    
    func isFavourite(articleId: String) -> Bool {
        return favouritesIds.contains(articleId)
    }
    
    func addToFavourites(articleId: String) {
        refresh { [weak self] in
            guard let self = self else { return }
            self.favouritesIds.insert(articleId)
            self.dataPersistingHandler.store(data: self.favouritesIds,
                                             key: Self.favouritesKey,
                                             completion: nil)
            self.onUpdate()
        }
    }
    
    func removeFromFavourites(articleId: String) {
        refresh { [weak self] in
            guard let self = self else { return }
            self.favouritesIds.remove(articleId)
            self.dataPersistingHandler.store(data: self.favouritesIds,
                                             key:  Self.favouritesKey,
                                             completion: nil)
            self.onUpdate()
        }
    }
}
