//
//  ArticlesContainerViewController.swift
//  Headlines
//
//  Created by khalifa on 13/08/2023.
//  Copyright © 2023 Example. All rights reserved.
//

import UIKit

final class ArticlesContainerViewController: UIPageViewController {
    var articels: [Article] = []
    var presentedByFavourites: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
    }
}

extension ArticlesContainerViewController {
    static func createViewController(articles: [Article] = [],
                                     visibleArticle: Article? = nil,
                                     presentedByFavourites: Bool = false) -> ArticlesContainerViewController {
        let viewController = ArticlesContainerViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: [:])
        let visibleController = ViewController.createArticleViewController()
        visibleController.article = visibleArticle
        viewController.setViewControllers([visibleController],
                                          direction: .forward,
                                          animated: true,
                                          completion: nil)
        viewController.articels = articles
        viewController.presentedByFavourites = presentedByFavourites
        visibleController.presentedByFavourites = presentedByFavourites
        return viewController
    }
}


extension ArticlesContainerViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerArticle = (viewController as? ViewController)?.article,
              let index = articels.firstIndex(where: {$0.id == viewControllerArticle.id}),
              index > 0 else {
            return nil
        }
        
        let viewController = ViewController.createArticleViewController(article: articels[index - 1], presentedByFavourites: presentedByFavourites)
        return viewController
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerArticle = (viewController as? ViewController)?.article,
              let index = articels.firstIndex(where: {$0.id == viewControllerArticle.id}),
              index < articels.count-1 else {
            return nil
        }
        
        return ViewController.createArticleViewController(article: articels[index + 1], presentedByFavourites: presentedByFavourites)
    }
}
